import { Document } from 'mongoose';
import { IPermission } from 'src/permission/interfaces';

export interface IRole extends Document {
    readonly name: string;
    readonly description: string;
    readonly permissions: IPermission[]
}