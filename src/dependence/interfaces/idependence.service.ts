import { IDependence } from './dependence.interface';

export interface IDependenceService {
    findAll(): Promise<IDependence[]>;
    findById(ID: number): Promise<IDependence | null>;
    findOne(options: object): Promise<IDependence | null>;
    create(todos: IDependence): Promise<IDependence>;
    update(ID: number, newValue: IDependence): Promise<IDependence | null>;
    delete(ID: number): Promise<string>;
}