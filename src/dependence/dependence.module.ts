import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { DependenceController } from './dependence.controller';
import { DependenceService } from './dependence.service';
import { DependenceSchema } from './schemas/dependence.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'Dependence', schema: DependenceSchema }])],
    controllers: [DependenceController],
    providers: [DependenceService],
})
export class DependenceModule {}