import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateSubsidyDto } from './dto/createSubsidy.dto';
import { debug } from 'console';
import { ISubsidyService, ISubsidy } from './interfaces';

@Injectable()
export class SubsidyService implements ISubsidyService {
    constructor(@InjectModel('Subsidy') private readonly subsidyModel: Model<ISubsidy>) { }

    async findAll(): Promise<ISubsidy[]> {
        return await this.subsidyModel.find().populate('subsidy_type').exec();
    }

    async findOne(options: object): Promise<ISubsidy> {
        return await this.subsidyModel.findOne(options).populate('subsidy_type').exec();
    }

    async findById(ID: number): Promise<ISubsidy> {
        return await this.subsidyModel.findById(ID).populate('subsidy_type').exec();
    }
    async create(createSubsidyDto: CreateSubsidyDto): Promise<ISubsidy> {
        const createdSubsidy = new this.subsidyModel(createSubsidyDto);
        return await createdSubsidy.save();
    }

    async update(ID: number, newValue: ISubsidy): Promise<ISubsidy> {
        const subsidy = await this.subsidyModel.findById(ID).exec();

        if (!subsidy._id) {
            debug('subsidy not found');
        }

        await this.subsidyModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.subsidyModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.subsidyModel.findByIdAndRemove(ID).exec();
            return 'The subsidy has been deleted';
        }
        catch (err){
            debug(err);
            return 'The subsidy could not be deleted';
        }
    }
}