import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateSurveyTypeDto } from './dto/createSurveyType.dto';
import { debug } from 'console';
import { ISurveyTypeService, ISurveyType } from './interfaces';

@Injectable()
export class SurveyTypeService implements ISurveyTypeService {
    constructor(@InjectModel('SurveyType') private readonly surveyTypeModel: Model<ISurveyType>) { }

    async findAll(): Promise<ISurveyType[]> {
        return await this.surveyTypeModel.find().exec();
    }

    async findOne(options: object): Promise<ISurveyType> {
        return await this.surveyTypeModel.findOne(options).exec();
    }

    async findById(ID: number): Promise<ISurveyType> {
        return await this.surveyTypeModel.findById(ID).exec();
    }
    async create(createSurveyTypeDto: CreateSurveyTypeDto): Promise<ISurveyType> {
        const createdSurveyType = new this.surveyTypeModel(createSurveyTypeDto);
        return await createdSurveyType.save();
    }

    async update(ID: number, newValue: ISurveyType): Promise<ISurveyType> {
        const surveyType = await this.surveyTypeModel.findById(ID).exec();

        if (!surveyType._id) {
            debug('surveyType not found');
        }

        await this.surveyTypeModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.surveyTypeModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.surveyTypeModel.findByIdAndRemove(ID).exec();
            return 'The surveyType has been deleted';
        }
        catch (err){
            debug(err);
            return 'The surveyType could not be deleted';
        }
    }
}