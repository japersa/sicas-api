import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { GenderService } from './gender.service';
import { CreateGenderDto } from './dto/createGender.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('genders')
@Controller('genders')
export class GenderController {
    constructor(private readonly genderService: GenderService) {}

    @Get()
    public async getCountries(@Response() res) {
        const gender = await this.genderService.findAll();
        return res.status(HttpStatus.OK).json(gender);
    }

    @Get('find')
    public async findGender(@Response() res, @Body() body) {
        const queryCondition = body;
        const gender = await this.genderService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(gender);
    }

    @Get('/:id')
    public async getGender(@Response() res, @Param() param){
        const gender = await this.genderService.findById(param.id);
        return res.status(HttpStatus.OK).json(gender);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createGender(@Response() res, @Body() createGenderDTO: CreateGenderDto) {

        const todo = await this.genderService.create(createGenderDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateGender(@Param() param, @Response() res, @Body() body) {

        const todo = await this.genderService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteGender(@Param() param, @Response() res) {

        const todo = await this.genderService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
