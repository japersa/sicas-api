import { Document } from 'mongoose';

export interface ISubsidyType extends Document {
    readonly description: string;
}