import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SubsidyTypeController } from './subsidy-type.controller';
import { SubsidyTypeService } from './subsidy-type.service';
import { SubsidyTypeSchema } from './schemas/subsidy-type.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'SubsidyType', schema: SubsidyTypeSchema }])],
    controllers: [SubsidyTypeController],
    providers: [SubsidyTypeService],
})
export class SubsidyTypeModule {}