import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { RoleService } from './role.service';
import { CreateRoleDto } from './dto/createRole.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('roles')
@Controller('roles')
export class RoleController {
    constructor(private readonly roleService: RoleService) {}

    @Get()
    public async getCountries(@Response() res) {
        const role = await this.roleService.findAll();
        return res.status(HttpStatus.OK).json(role);
    }

    @Get('find')
    public async findRole(@Response() res, @Body() body) {
        const queryCondition = body;
        const role = await this.roleService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(role);
    }

    @Get('/:id')
    public async getRole(@Response() res, @Param() param){
        const role = await this.roleService.findById(param.id);
        return res.status(HttpStatus.OK).json(role);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createRole(@Response() res, @Body() createRoleDTO: CreateRoleDto) {

        const todo = await this.roleService.create(createRoleDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateRole(@Param() param, @Response() res, @Body() body) {

        const todo = await this.roleService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteRole(@Param() param, @Response() res) {

        const todo = await this.roleService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
