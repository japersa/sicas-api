import { ApiModelProperty } from '@nestjs/swagger';

export class CreateCountryDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;
}