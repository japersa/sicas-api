import { Document } from 'mongoose';

export interface IFilter extends Document {
    readonly title: string;
    readonly description: string;
}