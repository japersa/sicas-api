import { Test, TestingModule } from '@nestjs/testing';
import { SidewalkService } from './sidewalk.service';

describe('SidewalkService', () => {
  let service: SidewalkService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SidewalkService],
    }).compile();

    service = module.get<SidewalkService>(SidewalkService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
