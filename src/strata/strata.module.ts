import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StrataController } from './strata.controller';
import { StrataService } from './strata.service';
import { StrataSchema } from './schemas/strata.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'Strata', schema: StrataSchema }])],
    controllers: [StrataController],
    providers: [StrataService],
})
export class StrataModule {}