import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateCountryDto } from './dto/createCountry.dto';
import { debug } from 'console';
import { ICountryService, ICountry } from './interfaces';

@Injectable()
export class CountryService implements ICountryService {
    constructor(@InjectModel('Country') private readonly countryModel: Model<ICountry>) { }

    async findAll(): Promise<ICountry[]> {
        return await this.countryModel.find().exec();
    }

    async findOne(options: object): Promise<ICountry> {
        return await this.countryModel.findOne(options).exec();
    }

    async findById(ID: number): Promise<ICountry> {
        return await this.countryModel.findById(ID).exec();
    }
    async create(createCountryDto: CreateCountryDto): Promise<ICountry> {
        const createdCountry = new this.countryModel(createCountryDto);
        return await createdCountry.save();
    }

    async update(ID: number, newValue: ICountry): Promise<ICountry> {
        const country = await this.countryModel.findById(ID).exec();

        if (!country._id) {
            debug('country not found');
        }

        await this.countryModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.countryModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.countryModel.findByIdAndRemove(ID).exec();
            return 'The country has been deleted';
        }
        catch (err){
            debug(err);
            return 'The country could not be deleted';
        }
    }
}