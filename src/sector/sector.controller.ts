import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { SectorService } from './sector.service';
import { CreateSectorDto } from './dto/createSector.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('sectors')
@Controller('sectors')
export class SectorController {
    constructor(private readonly sectorService: SectorService) {}

    @Get()
    public async getCountries(@Response() res) {
        const sector = await this.sectorService.findAll();
        return res.status(HttpStatus.OK).json(sector);
    }

    @Get('find')
    public async findSector(@Response() res, @Body() body) {
        const queryCondition = body;
        const sector = await this.sectorService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(sector);
    }

    @Get('/:id')
    public async getSector(@Response() res, @Param() param){
        const sector = await this.sectorService.findById(param.id);
        return res.status(HttpStatus.OK).json(sector);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createSector(@Response() res, @Body() createSectorDTO: CreateSectorDto) {

        const todo = await this.sectorService.create(createSectorDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateSector(@Param() param, @Response() res, @Body() body) {

        const todo = await this.sectorService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteSector(@Param() param, @Response() res) {

        const todo = await this.sectorService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
