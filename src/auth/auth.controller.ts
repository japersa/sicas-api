import { Controller, Get, Response, Headers, HttpStatus, Param, Body, Post, Request, Patch, Delete, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiUseTags, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Credentials } from './dto/credentials.dto';

@ApiUseTags('auth')
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}
	@Get('verify')
	@ApiBearerAuth()
	@UseGuards(AuthGuard('jwt'))
	public async verify(@Headers('Authorization') token: string) {
		return this.authService.verifyToken(token);
	}

	@Post()
	public async getToken(@Response() res, @Body() credentials: Credentials) {

        const user = await this.authService.createToken(credentials);;

        if(user){
            return res.status(HttpStatus.OK).json(user);
        }

        return res.status(HttpStatus.UNAUTHORIZED).json({ message: 'todo doesn\'t exist!' });

    }
    
}
