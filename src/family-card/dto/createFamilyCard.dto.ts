import { ApiModelProperty } from '@nestjs/swagger';
import { ICountry } from 'src/country/interfaces';
import { IDepartment } from 'src/department/interfaces';
import { ICity } from 'src/city/interfaces';
import { INeighborhood } from 'src/neighborhood/interfaces';
import { ICommune } from 'src/commune/interfaces';
import { ISidewalk } from 'src/sidewalk/interfaces';
import { ISector } from 'src/sector/interfaces';

export class CreateFamilyCardDto {
    @ApiModelProperty()
    readonly _id: number;
    @ApiModelProperty()
    readonly number_card: string;
    @ApiModelProperty()
    readonly country: ICountry;
    @ApiModelProperty()
    readonly department: IDepartment;
    @ApiModelProperty()
    readonly city: ICity;
    @ApiModelProperty()
    readonly commune: ICommune;
    @ApiModelProperty()
    readonly neighborhood: INeighborhood;
    @ApiModelProperty()
    readonly sidewalk: ISidewalk;
    @ApiModelProperty()
    readonly address: string;
    @ApiModelProperty()
    readonly latitude: string;
    @ApiModelProperty()
    readonly longitude: number;
    @ApiModelProperty()
    readonly phone: number;
    @ApiModelProperty()
    readonly status: boolean
    @ApiModelProperty()
    readonly sector: ISector
}