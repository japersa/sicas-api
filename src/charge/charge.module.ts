import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ChargeController } from './charge.controller';
import { ChargeService } from './charge.service';
import { ChargeSchema } from './schemas/charge.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'Charge', schema: ChargeSchema }])],
    controllers: [ChargeController],
    providers: [ChargeService],
})
export class ChargeModule {}