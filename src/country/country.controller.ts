import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { CountryService } from './country.service';
import { CreateCountryDto } from './dto/createCountry.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('countries')
@Controller('countries')
export class CountryController {
    constructor(private readonly countryService: CountryService) {}

    @Get()
    public async getCountries(@Response() res) {
        const country = await this.countryService.findAll();
        return res.status(HttpStatus.OK).json(country);
    }

    @Get('find')
    public async findCountry(@Response() res, @Body() body) {
        const queryCondition = body;
        const country = await this.countryService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(country);
    }

    @Get('/:id')
    public async getCountry(@Response() res, @Param() param){
        const country = await this.countryService.findById(param.id);
        return res.status(HttpStatus.OK).json(country);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createCountry(@Response() res, @Body() createCountryDTO: CreateCountryDto) {

        const todo = await this.countryService.create(createCountryDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateCountry(@Param() param, @Response() res, @Body() body) {

        const todo = await this.countryService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteCountry(@Param() param, @Response() res) {

        const todo = await this.countryService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
