import * as mongoose from 'mongoose';

export const SectorSchema = new mongoose.Schema({
    description: String
});