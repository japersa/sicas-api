import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { SurveyTypeService } from './survey-type.service';
import { CreateSurveyTypeDto } from './dto/createSurveyType.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('survey-types')
@Controller('survey-types')
export class SurveyTypeController {
    constructor(private readonly surveyTypeService: SurveyTypeService) {}

    @Get()
    public async getCountries(@Response() res) {
        const surveyType = await this.surveyTypeService.findAll();
        return res.status(HttpStatus.OK).json(surveyType);
    }

    @Get('find')
    public async findSurveyType(@Response() res, @Body() body) {
        const queryCondition = body;
        const surveyType = await this.surveyTypeService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(surveyType);
    }

    @Get('/:id')
    public async getSurveyType(@Response() res, @Param() param){
        const surveyType = await this.surveyTypeService.findById(param.id);
        return res.status(HttpStatus.OK).json(surveyType);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createSurveyType(@Response() res, @Body() createSurveyTypeDTO: CreateSurveyTypeDto) {

        const todo = await this.surveyTypeService.create(createSurveyTypeDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateSurveyType(@Param() param, @Response() res, @Body() body) {

        const todo = await this.surveyTypeService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteSurveyType(@Param() param, @Response() res) {

        const todo = await this.surveyTypeService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
