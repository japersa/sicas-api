import { Document } from 'mongoose';
import { ISurveyType } from 'src/survey-type/interfaces';

export interface ISurvey extends Document {
    readonly _id: number;
    readonly name: string;
    readonly description: string;
    readonly lock: boolean;
    readonly survey_type: ISurveyType;
    readonly access_key: string
}