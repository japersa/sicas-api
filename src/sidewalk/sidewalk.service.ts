import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateSidewalkDto } from './dto/createSidewalk.dto';
import { debug } from 'console';
import { ISidewalkService, ISidewalk } from './interfaces';

@Injectable()
export class SidewalkService implements ISidewalkService {
    constructor(@InjectModel('Sidewalk') private readonly sidewalkModel: Model<ISidewalk>) { }

    async findAll(): Promise<ISidewalk[]> {
        return await this.sidewalkModel.find().populate('city').exec();
    }

    async findOptions(options: object): Promise<ISidewalk[]> {
        return await this.sidewalkModel.find(options).populate('city').exec();
    }

    async findById(ID: number): Promise<ISidewalk> {
        return await this.sidewalkModel.findById(ID).populate('city').exec();
    }
    async create(createSidewalkDto: CreateSidewalkDto): Promise<ISidewalk> {
        const createdSidewalk = new this.sidewalkModel(createSidewalkDto);
        return await createdSidewalk.save();
    }

    async update(ID: number, newValue: ISidewalk): Promise<ISidewalk> {
        const sidewalk = await this.sidewalkModel.findById(ID).exec();

        if (!sidewalk._id) {
            debug('sidewalk not found');
        }

        await this.sidewalkModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.sidewalkModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.sidewalkModel.findByIdAndRemove(ID).exec();
            return 'The sidewalk has been deleted';
        }
        catch (err){
            debug(err);
            return 'The sidewalk could not be deleted';
        }
    }
}