import { IPermission } from './permission.interface';

export interface IPermissionService {
    findAll(): Promise<IPermission[]>;
    findById(ID: number): Promise<IPermission | null>;
    findOne(options: object): Promise<IPermission | null>;
    create(todos: IPermission): Promise<IPermission>;
    update(ID: number, newValue: IPermission): Promise<IPermission | null>;
    delete(ID: number): Promise<string>;
}