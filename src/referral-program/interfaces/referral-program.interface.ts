import { Document } from 'mongoose';

export interface IReferralProgram extends Document {
    readonly description: string;
}