import { Document } from 'mongoose';
import { ICharge } from 'src/charge/interfaces';

export interface IProfessional extends Document {
    readonly description: string;
    readonly charge: ICharge;
}