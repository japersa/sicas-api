import { Document } from 'mongoose';
import { ISurveyType } from 'src/survey-type/interfaces';

export interface ISubsidy extends Document {
    readonly description: string;
    readonly subsidy_type: ISurveyType;
}