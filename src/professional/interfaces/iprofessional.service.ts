import { IProfessional } from './professional.interface';

export interface IProfessionalService {
    findAll(): Promise<IProfessional[]>;
    findById(ID: number): Promise<IProfessional | null>;
    findOne(options: object): Promise<IProfessional | null>;
    create(todos: IProfessional): Promise<IProfessional>;
    update(ID: number, newValue: IProfessional): Promise<IProfessional | null>;
    delete(ID: number): Promise<string>;
}