import { Document } from 'mongoose';
import { ICommune } from 'src/commune/interfaces';

export interface INeighborhood extends Document {
    readonly description: string;
    readonly commune: ICommune;
}