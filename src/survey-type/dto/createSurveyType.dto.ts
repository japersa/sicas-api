import { ApiModelProperty } from '@nestjs/swagger';

export class CreateSurveyTypeDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

}