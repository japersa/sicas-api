import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FilterController } from './filter.controller';
import { FilterService } from './filter.service';
import { FilterSchema } from './schemas/filter.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'Filter', schema: FilterSchema }])],
    controllers: [FilterController],
    providers: [FilterService],
})
export class FilterModule {}