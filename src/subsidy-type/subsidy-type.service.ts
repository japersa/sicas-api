import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateSubsidyTypeDto } from './dto/createSubsidyType.dto';
import { debug } from 'console';
import { ISubsidyTypeService, ISubsidyType } from './interfaces';

@Injectable()
export class SubsidyTypeService implements ISubsidyTypeService {
    constructor(@InjectModel('SubsidyType') private readonly subsidyTypeModel: Model<ISubsidyType>) { }

    async findAll(): Promise<ISubsidyType[]> {
        return await this.subsidyTypeModel.find().exec();
    }

    async findOne(options: object): Promise<ISubsidyType> {
        return await this.subsidyTypeModel.findOne(options).exec();
    }

    async findById(ID: number): Promise<ISubsidyType> {
        return await this.subsidyTypeModel.findById(ID).exec();
    }
    async create(createSubsidyTypeDto: CreateSubsidyTypeDto): Promise<ISubsidyType> {
        const createdSubsidyType = new this.subsidyTypeModel(createSubsidyTypeDto);
        return await createdSubsidyType.save();
    }

    async update(ID: number, newValue: ISubsidyType): Promise<ISubsidyType> {
        const subsidyType = await this.subsidyTypeModel.findById(ID).exec();

        if (!subsidyType._id) {
            debug('subsidyType not found');
        }

        await this.subsidyTypeModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.subsidyTypeModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.subsidyTypeModel.findByIdAndRemove(ID).exec();
            return 'The subsidyType has been deleted';
        }
        catch (err){
            debug(err);
            return 'The subsidyType could not be deleted';
        }
    }
}