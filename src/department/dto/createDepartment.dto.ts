import { ApiModelProperty } from '@nestjs/swagger';
import { ICountry } from 'src/country/interfaces';

export class CreateDepartmentDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

    @ApiModelProperty()
    readonly country: ICountry;
}