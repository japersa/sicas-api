import { INeighborhood } from './neighborhood.interface';

export interface INeighborhoodService {
    findAll(): Promise<INeighborhood[]>;
    findById(ID: number): Promise<INeighborhood | null>;
    findOptions(options: object): Promise<INeighborhood[]>;
    create(todos: INeighborhood): Promise<INeighborhood>;
    update(ID: number, newValue: INeighborhood): Promise<INeighborhood | null>;
    delete(ID: number): Promise<string>;
}