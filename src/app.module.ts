import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CountryModule } from './country/country.module';
import { CityModule } from './city/city.module';
import { DepartmentModule } from './department/department.module';
import { RoleModule } from './role/role.module';
import { DependenceModule } from './dependence/dependence.module';
import { GenderModule } from './gender/gender.module';
import { CommuneModule } from './commune/commune.module';
import { ChargeModule } from './charge/charge.module';
import { UserModule } from './user/user.module';
import { PermissionModule } from './permission/permission.module';
import { StrataModule } from './strata/strata.module';
import { SidewalkModule } from './sidewalk/sidewalk.module';
import { ProfessionalModule } from './professional/professional.module';
import { ReferralProgramModule } from './referral-program/referral-program.module';
import { SectorModule } from './sector/sector.module';
import { SurveyModule } from './survey/survey.module';
import { SurveyTypeModule } from './survey-type/survey-type.module';
import { SubsidyModule } from './subsidy/subsidy.module';
import { SubsidyTypeModule } from './subsidy-type/subsidy-type.module';
import { NeighborhoodModule } from './neighborhood/neighborhood.module';
import { FamilyCardModule } from './family-card/family-card.module';
import { FilterModule } from './filter/filter.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://root:RuY18zKEWzWMlZjbb@cluster0-c0pbp.mongodb.net/test?retryWrites=true&w=majority' , {
      dbName: "test",
      autoReconnect: true,
      reconnectTries: Number.MAX_VALUE,
      reconnectInterval: 1000,
      useNewUrlParser: true ,
 }),
    ChargeModule,
    CityModule,
    CountryModule,
    CommuneModule,
    DependenceModule,
    DepartmentModule,
    GenderModule,
    PermissionModule,
    ProfessionalModule,
    ReferralProgramModule,
    RoleModule,
    SectorModule,
    StrataModule,
    SidewalkModule,
		AuthModule,
		UserModule,
    SurveyModule,
    SurveyTypeModule,
    SubsidyModule,
    SubsidyTypeModule,
    NeighborhoodModule,
    FilterModule,
    FamilyCardModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
