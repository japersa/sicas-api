import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateFamilyCardDto } from './dto/createFamilyCard.dto';
import { debug } from 'console';
import { IFamilyCardService, IFamilyCard } from './interfaces';

@Injectable()
export class FamilyCardService implements IFamilyCardService {
    constructor(@InjectModel('FamilyCard') private readonly familyCardModel: Model<IFamilyCard>) { }

    async findAll(): Promise<IFamilyCard[]> {
        return await this.familyCardModel.find().populate('country').populate('department').populate('city').populate('commune').populate('sidewalk').populate('neighborhood').populate('sector').exec();
    }

    async findOne(options: object): Promise<IFamilyCard> {
        return await this.familyCardModel.findOne(options).populate('country').populate('department').populate('city').populate('commune').populate('sidewalk').populate('neighborhood').populate('sector').exec();
    }

    async findById(ID: number): Promise<IFamilyCard> {
        return await this.familyCardModel.findById(ID).populate('country').populate('department').populate('city').populate('commune').populate('sidewalk').populate('neighborhood').populate('sector').exec();
    }
    async create(createFamilyCardDto: CreateFamilyCardDto): Promise<IFamilyCard> {
        const createdFamilyCard = new this.familyCardModel(createFamilyCardDto);
        return await createdFamilyCard.save();
    }

    async update(ID: number, newValue: IFamilyCard): Promise<IFamilyCard> {
        const familyCard = await this.familyCardModel.findById(ID).exec();

        if (!familyCard._id) {
            debug('familyCard not found');
        }

        await this.familyCardModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.familyCardModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.familyCardModel.findByIdAndRemove(ID).exec();
            return 'The familyCard has been deleted';
        }
        catch (err){
            debug(err);
            return 'The familyCard could not be deleted';
        }
    }
}