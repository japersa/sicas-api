import { Document } from 'mongoose';
import { ICity } from 'src/city/interfaces';

export interface ICommune extends Document {
    readonly description: string;
    readonly city: ICity;
}