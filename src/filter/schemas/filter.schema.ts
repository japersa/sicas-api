import * as mongoose from 'mongoose';

export const FilterSchema = new mongoose.Schema({
    title: String,
    description: String
});