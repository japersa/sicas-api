import { ApiModelProperty } from '@nestjs/swagger';
import { ICharge } from 'src/charge/interfaces';

export class CreateProfessionalDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

    @ApiModelProperty()
    readonly charge: ICharge;
}