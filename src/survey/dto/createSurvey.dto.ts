import { ApiModelProperty } from '@nestjs/swagger';
import { ISurveyType } from 'src/survey-type/interfaces';

export class CreateSurveyDto {
    @ApiModelProperty()
    readonly _id: number;
    @ApiModelProperty()
    readonly name: string;
     @ApiModelProperty()
    readonly description: string;
     @ApiModelProperty()
    readonly lock: boolean;
    @ApiModelProperty()
    readonly survey_type: ISurveyType;
     @ApiModelProperty()
    readonly access_key: string
}