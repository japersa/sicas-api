import { ICity } from './city.interface';

export interface ICityService {
    findAll(): Promise<ICity[]>;
    findById(ID: number): Promise<ICity | null>;
    findOptions(options: object): Promise<ICity[]>;
    create(todos: ICity): Promise<ICity>;
    update(ID: number, newValue: ICity): Promise<ICity | null>;
    delete(ID: number): Promise<string>;
}