import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateSectorDto } from './dto/createSector.dto';
import { debug } from 'console';
import { ISectorService, ISector } from './interfaces';

@Injectable()
export class SectorService implements ISectorService {
    constructor(@InjectModel('Sector') private readonly sectorModel: Model<ISector>) { }

    async findAll(): Promise<ISector[]> {
        return await this.sectorModel.find().exec();
    }

    async findOne(options: object): Promise<ISector> {
        return await this.sectorModel.findOne(options).exec();
    }

    async findById(ID: number): Promise<ISector> {
        return await this.sectorModel.findById(ID).exec();
    }
    async create(createSectorDto: CreateSectorDto): Promise<ISector> {
        const createdSector = new this.sectorModel(createSectorDto);
        return await createdSector.save();
    }

    async update(ID: number, newValue: ISector): Promise<ISector> {
        const sector = await this.sectorModel.findById(ID).exec();

        if (!sector._id) {
            debug('sector not found');
        }

        await this.sectorModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.sectorModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.sectorModel.findByIdAndRemove(ID).exec();
            return 'The sector has been deleted';
        }
        catch (err){
            debug(err);
            return 'The sector could not be deleted';
        }
    }
}