import { Test, TestingModule } from '@nestjs/testing';
import { SurveyTypeService } from './survey-type.service';

describe('SurveyTypeService', () => {
  let service: SurveyTypeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SurveyTypeService],
    }).compile();

    service = module.get<SurveyTypeService>(SurveyTypeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
