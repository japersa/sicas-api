import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FamilyCardController } from './family-card.controller';
import { FamilyCardService } from './family-card.service';
import { FamilyCardSchema } from './schemas/family-card.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'FamilyCard', schema: FamilyCardSchema }])],
    controllers: [FamilyCardController],
    providers: [FamilyCardService],
})
export class FamilyCardModule {}