import * as mongoose from 'mongoose';

export const CitySchema = new mongoose.Schema({
    description: String,
    department: { type: mongoose.Schema.Types.ObjectId, ref: 'Department' },
});