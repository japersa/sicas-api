import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { SubsidyService } from './subsidy.service';
import { CreateSubsidyDto } from './dto/createSubsidy.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('subsidies')
@Controller('subsidies')
export class SubsidyController {
    constructor(private readonly subsidyService: SubsidyService) {}

    @Get()
    public async getSubsidies(@Response() res) {
        const subsidy = await this.subsidyService.findAll();
        return res.status(HttpStatus.OK).json(subsidy);
    }

    @Get('find')
    public async findSubsidy(@Response() res, @Body() body) {
        const queryCondition = body;
        const subsidy = await this.subsidyService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(subsidy);
    }

    @Get('/:id')
    public async getSubsidy(@Response() res, @Param() param){
        const subsidy = await this.subsidyService.findById(param.id);
        return res.status(HttpStatus.OK).json(subsidy);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createSubsidy(@Response() res, @Body() createSubsidyDTO: CreateSubsidyDto) {

        const todo = await this.subsidyService.create(createSubsidyDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateSubsidy(@Param() param, @Response() res, @Body() body) {

        const todo = await this.subsidyService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteSubsidy(@Param() param, @Response() res) {

        const todo = await this.subsidyService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
