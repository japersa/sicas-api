import { ISector } from './sector.interface';

export interface ISectorService {
    findAll(): Promise<ISector[]>;
    findById(ID: number): Promise<ISector | null>;
    findOne(options: object): Promise<ISector | null>;
    create(todos: ISector): Promise<ISector>;
    update(ID: number, newValue: ISector): Promise<ISector | null>;
    delete(ID: number): Promise<string>;
}