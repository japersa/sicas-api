import * as mongoose from 'mongoose';

export const SurveySchema = new mongoose.Schema({
    name: String,
    description: String,
    lock: String,
    survey_type: { type: mongoose.Schema.Types.ObjectId, ref: 'SurveyType' },
    access_key: String
});