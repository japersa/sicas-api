import { ICountry } from './country.interface';

export interface ICountryService {
    findAll(): Promise<ICountry[]>;
    findById(ID: number): Promise<ICountry | null>;
    findOne(options: object): Promise<ICountry | null>;
    create(todos: ICountry): Promise<ICountry>;
    update(ID: number, newValue: ICountry): Promise<ICountry | null>;
    delete(ID: number): Promise<string>;
}