import { Test, TestingModule } from '@nestjs/testing';
import { SubsidyTypeController } from './subsidy-type.controller';

describe('SubsidyType Controller', () => {
  let controller: SubsidyTypeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SubsidyTypeController],
    }).compile();

    controller = module.get<SubsidyTypeController>(SubsidyTypeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
