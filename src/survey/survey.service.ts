import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateSurveyDto } from './dto/createSurvey.dto';
import { debug } from 'console';
import { ISurveyService, ISurvey } from './interfaces';

@Injectable()
export class SurveyService implements ISurveyService {
    constructor(@InjectModel('Survey') private readonly surveyModel: Model<ISurvey>) { }

    async findAll(): Promise<ISurvey[]> {
        return await this.surveyModel.find().populate('survey_type').exec();
    }

    async findOne(options: object): Promise<ISurvey> {
        return await this.surveyModel.findOne(options).populate('survey_type').exec();
    }

    async findById(ID: number): Promise<ISurvey> {
        return await this.surveyModel.findById(ID).populate('survey_type').exec();
    }
    async create(createSurveyDto: CreateSurveyDto): Promise<ISurvey> {
        const createdSurvey = new this.surveyModel(createSurveyDto);
        return await createdSurvey.save();
    }

    async update(ID: number, newValue: ISurvey): Promise<ISurvey> {
        const survey = await this.surveyModel.findById(ID).exec();

        if (!survey._id) {
            debug('survey not found');
        }

        await this.surveyModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.surveyModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.surveyModel.findByIdAndRemove(ID).exec();
            return 'The survey has been deleted';
        }
        catch (err){
            debug(err);
            return 'The survey could not be deleted';
        }
    }
}