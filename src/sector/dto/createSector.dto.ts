import { ApiModelProperty } from '@nestjs/swagger';

export class CreateSectorDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

}