import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { PermissionService } from './permission.service';
import { CreatePermissionDto } from './dto/createPermission.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('permissions')
@Controller('permissions')
export class PermissionController {
    constructor(private readonly permissionService: PermissionService) {}

    @Get()
    public async getCountries(@Response() res) {
        const permission = await this.permissionService.findAll();
        return res.status(HttpStatus.OK).json(permission);
    }

    @Get('find')
    public async findPermission(@Response() res, @Body() body) {
        const queryCondition = body;
        const permission = await this.permissionService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(permission);
    }

    @Get('/:id')
    public async getPermission(@Response() res, @Param() param){
        const permission = await this.permissionService.findById(param.id);
        return res.status(HttpStatus.OK).json(permission);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createPermission(@Response() res, @Body() createPermissionDTO: CreatePermissionDto) {

        const todo = await this.permissionService.create(createPermissionDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updatePermission(@Param() param, @Response() res, @Body() body) {

        const todo = await this.permissionService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deletePermission(@Param() param, @Response() res) {

        const todo = await this.permissionService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
