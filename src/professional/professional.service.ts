import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateProfessionalDto } from './dto/createProfessional.dto';
import { debug } from 'console';
import { IProfessionalService, IProfessional } from './interfaces';

@Injectable()
export class ProfessionalService implements IProfessionalService {
    constructor(@InjectModel('Professional') private readonly professionalModel: Model<IProfessional>) { }

    async findAll(): Promise<IProfessional[]> {
        return await this.professionalModel.find().populate('charge').exec();
    }

    async findOne(options: object): Promise<IProfessional> {
        return await this.professionalModel.findOne(options).populate('charge').exec();
    }

    async findById(ID: number): Promise<IProfessional> {
        return await this.professionalModel.findById(ID).populate('charge').exec();
    }
    async create(createProfessionalDto: CreateProfessionalDto): Promise<IProfessional> {
        const createdProfessional = new this.professionalModel(createProfessionalDto);
        return await createdProfessional.save();
    }

    async update(ID: number, newValue: IProfessional): Promise<IProfessional> {
        const professional = await this.professionalModel.findById(ID).exec();

        if (!professional._id) {
            debug('professional not found');
        }

        await this.professionalModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.professionalModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.professionalModel.findByIdAndRemove(ID).exec();
            return 'The professional has been deleted';
        }
        catch (err){
            debug(err);
            return 'The professional could not be deleted';
        }
    }
}