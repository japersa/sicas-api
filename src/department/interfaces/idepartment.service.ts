import { IDepartment } from './department.interface';

export interface IDepartmentService {
    findAll(): Promise<IDepartment[]>;
    findById(ID: number): Promise<IDepartment | null>;
    findOptions(options: object): Promise<IDepartment[]>;
    create(todos: IDepartment): Promise<IDepartment>;
    update(ID: number, newValue: IDepartment): Promise<IDepartment | null>;
    delete(ID: number): Promise<string>;
}