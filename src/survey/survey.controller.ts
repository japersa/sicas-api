import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { SurveyService } from './survey.service';
import { CreateSurveyDto } from './dto/createSurvey.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('surveys')
@Controller('surveys')
export class SurveyController {
    constructor(private readonly surveyService: SurveyService) {}

    @Get()
    public async getSurveys(@Response() res) {
        const survey = await this.surveyService.findAll();
        return res.status(HttpStatus.OK).json(survey);
    }

    @Get('find')
    public async findSurvey(@Response() res, @Body() body) {
        const queryCondition = body;
        const survey = await this.surveyService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(survey);
    }

    @Get('/:id')
    public async getSurvey(@Response() res, @Param() param){
        const survey = await this.surveyService.findById(param.id);
        return res.status(HttpStatus.OK).json(survey);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createSurvey(@Response() res, @Body() createSurveyDTO: CreateSurveyDto) {

        const todo = await this.surveyService.create(createSurveyDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateSurvey(@Param() param, @Response() res, @Body() body) {

        const todo = await this.surveyService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteSurvey(@Param() param, @Response() res) {

        const todo = await this.surveyService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
