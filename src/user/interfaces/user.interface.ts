import { Document } from 'mongoose';
import { IRole } from 'src/role/interfaces';

export interface IUser extends Document {
    readonly name: string;
    readonly email: string;
    readonly password: string;
    readonly roles: IRole[];
}