import * as mongoose from 'mongoose';

export const DepartmentSchema = new mongoose.Schema({
    description: String,
    country: { type: mongoose.Schema.Types.ObjectId, ref: 'Country' },
});