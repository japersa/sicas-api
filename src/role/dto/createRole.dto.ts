import { ApiModelProperty } from '@nestjs/swagger';
import { IPermission } from 'src/permission/interfaces';

export class CreateRoleDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly name: string;

    @ApiModelProperty()
    readonly description: string;

    @ApiModelProperty()
    readonly permissions: IPermission[];
}