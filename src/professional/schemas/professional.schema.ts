import * as mongoose from 'mongoose';

export const ProfessionalSchema = new mongoose.Schema({
    description: String,
    charge: { type: mongoose.Schema.Types.ObjectId, ref: 'Charge' },
});