import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateChargeDto } from './dto/createCharge.dto';
import { debug } from 'console';
import { IChargeService, ICharge } from './interfaces';

@Injectable()
export class ChargeService implements IChargeService {
    constructor(@InjectModel('Charge') private readonly chargeModel: Model<ICharge>) { }

    async findAll(): Promise<ICharge[]> {
        return await this.chargeModel.find().exec();
    }

    async findOne(options: object): Promise<ICharge> {
        return await this.chargeModel.findOne(options).exec();
    }

    async findById(ID: number): Promise<ICharge> {
        return await this.chargeModel.findById(ID).exec();
    }
    async create(createChargeDto: CreateChargeDto): Promise<ICharge> {
        const createdCharge = new this.chargeModel(createChargeDto);
        return await createdCharge.save();
    }

    async update(ID: number, newValue: ICharge): Promise<ICharge> {
        const charge = await this.chargeModel.findById(ID).exec();

        if (!charge._id) {
            debug('charge not found');
        }

        await this.chargeModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.chargeModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.chargeModel.findByIdAndRemove(ID).exec();
            return 'The charge has been deleted';
        }
        catch (err){
            debug(err);
            return 'The charge could not be deleted';
        }
    }
}