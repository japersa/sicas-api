import * as mongoose from 'mongoose';

export const PermissionSchema = new mongoose.Schema({
    name: String,
    description: String,
});