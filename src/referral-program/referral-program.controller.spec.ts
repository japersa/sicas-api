import { Test, TestingModule } from '@nestjs/testing';
import { ReferralProgramController } from './referral-program.controller';

describe('ReferralProgram Controller', () => {
  let controller: ReferralProgramController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReferralProgramController],
    }).compile();

    controller = module.get<ReferralProgramController>(ReferralProgramController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
