import { ApiModelProperty } from '@nestjs/swagger';
import { ICity } from 'src/city/interfaces';

export class CreateCommuneDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

    @ApiModelProperty()
    readonly city: ICity;
}