import { Document } from 'mongoose';

export interface ISector extends Document {
    readonly description: string;
}