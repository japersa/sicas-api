import { ApiModelProperty } from '@nestjs/swagger';

export class CreateFilterDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly title: string;

    @ApiModelProperty()
    readonly description: string;
}