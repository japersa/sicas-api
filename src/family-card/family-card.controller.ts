import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { FamilyCardService } from './family-card.service';
import { CreateFamilyCardDto } from './dto/createFamilyCard.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('family-cards')
@Controller('family-cards')
export class FamilyCardController {
    constructor(private readonly familyCardService: FamilyCardService) {}

    @Get()
    public async getFamilyCards(@Response() res) {
        const familyCard = await this.familyCardService.findAll();
        return res.status(HttpStatus.OK).json(familyCard);
    }

    @Get('find')
    public async findFamilyCard(@Response() res, @Body() body) {
        const queryCondition = body;
        const familyCard = await this.familyCardService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(familyCard);
    }

    @Get('/:id')
    public async getFamilyCard(@Response() res, @Param() param){
        const familyCard = await this.familyCardService.findById(param.id);
        return res.status(HttpStatus.OK).json(familyCard);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createFamilyCard(@Response() res, @Body() createFamilyCardDTO: CreateFamilyCardDto) {

        const todo = await this.familyCardService.create(createFamilyCardDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateFamilyCard(@Param() param, @Response() res, @Body() body) {

        const todo = await this.familyCardService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteFamilyCard(@Param() param, @Response() res) {

        const todo = await this.familyCardService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
