import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { CreateReferralProgramDto } from './dto/createReferralProgram.dto';
import { ReferralProgramService } from './referral-program.service';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('referralPrograms')
@Controller('referralPrograms')
export class ReferralProgramController {
    constructor(private readonly referralProgramService: ReferralProgramService) {}

    @Get()
    public async getCountries(@Response() res) {
        const referralProgram = await this.referralProgramService.findAll();
        return res.status(HttpStatus.OK).json(referralProgram);
    }

    @Get('find')
    public async findReferralProgram(@Response() res, @Body() body) {
        const queryCondition = body;
        const referralProgram = await this.referralProgramService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(referralProgram);
    }

    @Get('/:id')
    public async getReferralProgram(@Response() res, @Param() param){
        const referralProgram = await this.referralProgramService.findById(param.id);
        return res.status(HttpStatus.OK).json(referralProgram);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createReferralProgram(@Response() res, @Body() createReferralProgramDTO: CreateReferralProgramDto) {

        const todo = await this.referralProgramService.create(createReferralProgramDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateReferralProgram(@Param() param, @Response() res, @Body() body) {

        const todo = await this.referralProgramService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteReferralProgram(@Param() param, @Response() res) {

        const todo = await this.referralProgramService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
