import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SurveyTypeController } from './survey-type.controller';
import { SurveyTypeService } from './survey-type.service';
import { SurveyTypeSchema } from './schemas/survey-type.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'SurveyType', schema: SurveyTypeSchema }])],
    controllers: [SurveyTypeController],
    providers: [SurveyTypeService],
})
export class SurveyTypeModule {}