import { IRole } from './role.interface';

export interface IRoleService {
    findAll(): Promise<IRole[]>;
    findById(ID: number): Promise<IRole | null>;
    findOne(options: object): Promise<IRole | null>;
    create(todos: IRole): Promise<IRole>;
    update(ID: number, newValue: IRole): Promise<IRole | null>;
    delete(ID: number): Promise<string>;
}