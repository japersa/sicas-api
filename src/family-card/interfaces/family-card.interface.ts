import { Document } from 'mongoose';
import { ICountry } from 'src/country/interfaces';
import { IDepartment } from 'src/department/interfaces';
import { ICity } from 'src/city/interfaces';
import { INeighborhood } from 'src/neighborhood/interfaces';
import { ICommune } from 'src/commune/interfaces';
import { ISidewalk } from 'src/sidewalk/interfaces';
import { ISector } from 'src/sector/interfaces';

export interface IFamilyCard extends Document {
    readonly number_card: string;
    readonly country: ICountry;
    readonly department: IDepartment;
    readonly city: ICity;
    readonly commune: ICommune;
    readonly neighborhood: INeighborhood;
    readonly sidewalk: ISidewalk
    readonly address: string;
    readonly latitude: string;
    readonly longitude: number;
    readonly phone: number;
    readonly status: boolean
    readonly sector: ISector
}