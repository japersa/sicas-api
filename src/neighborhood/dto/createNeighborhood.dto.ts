import { ApiModelProperty } from '@nestjs/swagger';
import { ICountry } from 'src/country/interfaces';
import { ICommune } from 'src/commune/interfaces';

export class CreateNeighborhoodDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

    @ApiModelProperty()
    readonly commune: ICommune;
}