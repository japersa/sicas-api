import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateReferralProgramDto } from './dto/createReferralProgram.dto';
import { debug } from 'console';
import { IReferralProgramService, IReferralProgram } from './interfaces';

@Injectable()
export class ReferralProgramService implements IReferralProgramService {
    constructor(@InjectModel('ReferralProgram') private readonly referralProgramModel: Model<IReferralProgram>) { }

    async findAll(): Promise<IReferralProgram[]> {
        return await this.referralProgramModel.find().exec();
    }

    async findOne(options: object): Promise<IReferralProgram> {
        return await this.referralProgramModel.findOne(options).exec();
    }

    async findById(ID: number): Promise<IReferralProgram> {
        return await this.referralProgramModel.findById(ID).exec();
    }
    async create(createReferralProgramDto: CreateReferralProgramDto): Promise<IReferralProgram> {
        const createdReferralProgram = new this.referralProgramModel(createReferralProgramDto);
        return await createdReferralProgram.save();
    }

    async update(ID: number, newValue: IReferralProgram): Promise<IReferralProgram> {
        const referralProgram = await this.referralProgramModel.findById(ID).exec();

        if (!referralProgram._id) {
            debug('referral-program not found');
        }

        await this.referralProgramModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.referralProgramModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.referralProgramModel.findByIdAndRemove(ID).exec();
            return 'The referral program has been deleted';
        }
        catch (err){
            debug(err);
            return 'The referral program could not be deleted';
        }
    }
}