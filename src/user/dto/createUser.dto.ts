import { ApiModelProperty } from '@nestjs/swagger';
import { IRole } from 'src/role/interfaces';

export class CreateUserDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly name: string;

    @ApiModelProperty()
    readonly email: string;

    @ApiModelProperty()
    readonly password: string;

    @ApiModelProperty()
    readonly roles: IRole[];
    
}