import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NeighborhoodController } from './neighborhood.controller';
import { NeighborhoodService } from './neighborhood.service';
import { NeighborhoodSchema } from './schemas/neighborhood.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'Neighborhood', schema: NeighborhoodSchema }])],
    controllers: [NeighborhoodController],
    providers: [NeighborhoodService],
})
export class NeighborhoodModule {}