import { ISubsidy } from './subsidy.interface';

export interface ISubsidyService {
    findAll(): Promise<ISubsidy[]>;
    findById(ID: number): Promise<ISubsidy | null>;
    findOne(options: object): Promise<ISubsidy | null>;
    create(todos: ISubsidy): Promise<ISubsidy>;
    update(ID: number, newValue: ISubsidy): Promise<ISubsidy | null>;
    delete(ID: number): Promise<string>;
}