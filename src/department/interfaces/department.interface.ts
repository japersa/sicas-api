import { Document } from 'mongoose';
import { ICountry } from 'src/country/interfaces';

export interface IDepartment extends Document {
    readonly description: string;
    readonly country: ICountry;
}