import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { FilterService } from './filter.service';
import { CreateFilterDto } from './dto/createFilter.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('filters')
@Controller('filters')
export class FilterController {
    constructor(private readonly filterService: FilterService) {}

    @Get()
    public async getFilters(@Response() res) {
        const filter = await this.filterService.findAll();
        return res.status(HttpStatus.OK).json(filter);
    }

    @Get('find')
    public async findFilter(@Response() res, @Body() body) {
        const queryCondition = body;
        const filter = await this.filterService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(filter);
    }

    @Get('/:id')
    public async getFilter(@Response() res, @Param() param){
        const filter = await this.filterService.findById(param.id);
        return res.status(HttpStatus.OK).json(filter);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createFilter(@Response() res, @Body() createFilterDTO: CreateFilterDto) {

        const todo = await this.filterService.create(createFilterDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateFilter(@Param() param, @Response() res, @Body() body) {

        const todo = await this.filterService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteFilter(@Param() param, @Response() res) {

        const todo = await this.filterService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
