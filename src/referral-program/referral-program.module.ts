import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ReferralProgramController } from './referral-program.controller';
import { ReferralProgramService } from './referral-program.service';
import { ReferralProgramSchema } from './schemas/referral-program.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'ReferralProgram', schema: ReferralProgramSchema }])],
    controllers: [ReferralProgramController],
    providers: [ReferralProgramService],
})
export class ReferralProgramModule {}