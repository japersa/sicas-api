import { ApiModelProperty } from '@nestjs/swagger';

export class CreateDependenceDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

}