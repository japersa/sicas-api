import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GenderController } from './gender.controller';
import { GenderService } from './gender.service';
import { GenderSchema } from './schemas/gender.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'Gender', schema: GenderSchema }])],
    controllers: [GenderController],
    providers: [GenderService],
})
export class GenderModule {}