import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateFilterDto } from './dto/createFilter.dto';
import { debug } from 'console';
import { IFilterService, IFilter } from './interfaces';

@Injectable()
export class FilterService implements IFilterService {
  constructor(@InjectModel('Filter') private readonly filterModel: Model<IFilter>) { }

  async findAll(): Promise<IFilter[]> {
      return await this.filterModel.find().exec();
  }

  async findOne(options: object): Promise<IFilter> {
      return await this.filterModel.findOne(options).exec();
  }

  async findById(ID: number): Promise<IFilter> {
      return await this.filterModel.findById(ID).exec();
  }
  async create(createFilterDto: CreateFilterDto): Promise<IFilter> {
      const createdFilter = new this.filterModel(createFilterDto);
      return await createdFilter.save();
  }

  async update(ID: number, newValue: IFilter): Promise<IFilter> {
      const filter = await this.filterModel.findById(ID).exec();

      if (!filter._id) {
          debug('filter not found');
      }

      await this.filterModel.findByIdAndUpdate(ID, newValue).exec();
      return await this.filterModel.findById(ID).exec();
  }
  async delete(ID: number): Promise<string> {
      try {
          await this.filterModel.findByIdAndRemove(ID).exec();
          return 'The filter has been deleted';
      }
      catch (err){
          debug(err);
          return 'The filter could not be deleted';
      }
  }

}
