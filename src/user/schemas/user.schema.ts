import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
    name: String,
    email: String,
    password: String,
    roles: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Role' }]
});