import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SubsidyController } from './subsidy.controller';
import { SubsidyService } from './subsidy.service';
import { SubsidySchema } from './schemas/subsidy.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'Subsidy', schema: SubsidySchema }])],
    controllers: [SubsidyController],
    providers: [SubsidyService],
})
export class SubsidyModule {}