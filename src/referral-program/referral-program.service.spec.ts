import { Test, TestingModule } from '@nestjs/testing';
import { ReferralProgramService } from './referral-program.service';

describe('ReferralProgramService', () => {
  let service: ReferralProgramService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReferralProgramService],
    }).compile();

    service = module.get<ReferralProgramService>(ReferralProgramService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
