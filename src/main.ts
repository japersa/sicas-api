import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const options = new DocumentBuilder()
    .setTitle('Sicas')
    .setVersion('1.0')
    .addTag('countries')
    .addTag('charges')
    .addTag('cities')
    .addTag('countries')
    .addTag('communes')
    .addTag('dependences')
    .addTag('departments')
    .addTag('genders')
    .addTag('permissions')
    .addTag('professionals')
    .addTag('referralPrograms')
    .addTag('roles')
    .addTag('sectors')
    .addTag('stratas')
    .addTag('sidewalks')
    .addTag('users')
    .addTag('filters')
 .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document);
  app.enableCors();
  await app.listen(process.env.PORT || 3000);
}
bootstrap();