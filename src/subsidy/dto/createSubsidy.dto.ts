import { ApiModelProperty } from '@nestjs/swagger';
import { IDepartment } from 'src/department/interfaces';
import { ISubsidyType } from 'src/subsidy-type/interfaces';

export class CreateSubsidyDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

    @ApiModelProperty()
    readonly subsidy_type: ISubsidyType;
}