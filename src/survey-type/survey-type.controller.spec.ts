import { Test, TestingModule } from '@nestjs/testing';
import { SurveyTypeController } from './survey-type.controller';

describe('SurveyType Controller', () => {
  let controller: SurveyTypeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SurveyTypeController],
    }).compile();

    controller = module.get<SurveyTypeController>(SurveyTypeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
