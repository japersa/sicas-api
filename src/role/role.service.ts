import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateRoleDto } from './dto/createRole.dto';
import { debug } from 'console';
import { IRoleService, IRole } from './interfaces';

@Injectable()
export class RoleService implements IRoleService {
    constructor(@InjectModel('Role') private readonly roleModel: Model<IRole>) { }

    async findAll(): Promise<IRole[]> {
        return await this.roleModel.find().populate('permissions').exec();
    }

    async findOne(options: object): Promise<IRole> {
        return await this.roleModel.findOne(options).populate('permissions').exec();
    }

    async findById(ID: number): Promise<IRole> {
        return await this.roleModel.findById(ID).populate('permissions').exec();
    }
    async create(createRoleDto: CreateRoleDto): Promise<IRole> {
        const createdRole = new this.roleModel(createRoleDto);
        return await createdRole.save();
    }

    async update(ID: number, newValue: IRole): Promise<IRole> {
        const role = await this.roleModel.findById(ID).exec();

        if (!role._id) {
            debug('role not found');
        }

        await this.roleModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.roleModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.roleModel.findByIdAndRemove(ID).exec();
            return 'The role has been deleted';
        }
        catch (err){
            debug(err);
            return 'The role could not be deleted';
        }
    }
}