import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SidewalkController } from './sidewalk.controller';
import { SidewalkService } from './sidewalk.service';
import { SidewalkSchema } from './schemas/sidewalk.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'Sidewalk', schema: SidewalkSchema }])],
    controllers: [SidewalkController],
    providers: [SidewalkService],
})
export class SidewalkModule {}