import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreatePermissionDto } from './dto/createPermission.dto';
import { debug } from 'console';
import { IPermissionService, IPermission } from './interfaces';

@Injectable()
export class PermissionService implements IPermissionService {
    constructor(@InjectModel('Permission') private readonly permissionModel: Model<IPermission>) { }

    async findAll(): Promise<IPermission[]> {
        return await this.permissionModel.find().exec();
    }

    async findOne(options: object): Promise<IPermission> {
        return await this.permissionModel.findOne(options).exec();
    }

    async findById(ID: number): Promise<IPermission> {
        return await this.permissionModel.findById(ID).exec();
    }
    async create(createPermissionDto: CreatePermissionDto): Promise<IPermission> {
        const createdPermission = new this.permissionModel(createPermissionDto);
        return await createdPermission.save();
    }

    async update(ID: number, newValue: IPermission): Promise<IPermission> {
        const permission = await this.permissionModel.findById(ID).exec();

        if (!permission._id) {
            debug('permission not found');
        }

        await this.permissionModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.permissionModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.permissionModel.findByIdAndRemove(ID).exec();
            return 'The permission has been deleted';
        }
        catch (err){
            debug(err);
            return 'The permission could not be deleted';
        }
    }
}