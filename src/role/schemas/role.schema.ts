import * as mongoose from 'mongoose';

export const RoleSchema = new mongoose.Schema({
    name: String,
    description: String,
    permissions: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Permission' }]
});