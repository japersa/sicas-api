import { IStrata } from './strata.interface';

export interface IStrataService {
    findAll(): Promise<IStrata[]>;
    findById(ID: number): Promise<IStrata | null>;
    findOne(options: object): Promise<IStrata | null>;
    create(todos: IStrata): Promise<IStrata>;
    update(ID: number, newValue: IStrata): Promise<IStrata | null>;
    delete(ID: number): Promise<string>;
}