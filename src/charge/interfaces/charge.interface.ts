import { Document } from 'mongoose';

export interface ICharge extends Document {
    readonly description: string;
}