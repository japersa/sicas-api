import { Document } from 'mongoose';

export interface IDependence extends Document {
    readonly description: string;
}