import { Test, TestingModule } from '@nestjs/testing';
import { SidewalkController } from './sidewalk.controller';

describe('Sidewalk Controller', () => {
  let controller: SidewalkController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SidewalkController],
    }).compile();

    controller = module.get<SidewalkController>(SidewalkController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
