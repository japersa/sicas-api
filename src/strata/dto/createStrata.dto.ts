import { ApiModelProperty } from '@nestjs/swagger';

export class CreateStrataDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

}