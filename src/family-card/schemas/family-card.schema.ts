import * as mongoose from 'mongoose';

export const FamilyCardSchema = new mongoose.Schema({
    number_card: String,
    country: { type: mongoose.Schema.Types.ObjectId, ref: 'Country' },
    department: { type: mongoose.Schema.Types.ObjectId, ref: 'Department' },
    city: { type: mongoose.Schema.Types.ObjectId, ref: 'City' },
    commune: { type: mongoose.Schema.Types.ObjectId, ref: 'Commune' },
    neighborhood: { type: mongoose.Schema.Types.ObjectId, ref: 'Neighborhood' },
    sidewalk: { type: mongoose.Schema.Types.ObjectId, ref: 'Sidewalk' },
    address: String,
    latitude: Number,
    longitude: Number,
    phone: String,
    status: Boolean,
    sector: { type: mongoose.Schema.Types.ObjectId, ref: 'Sector' },
});