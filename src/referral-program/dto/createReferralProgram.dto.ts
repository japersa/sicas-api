import { ApiModelProperty } from '@nestjs/swagger';

export class CreateReferralProgramDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

}