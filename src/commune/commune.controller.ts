import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { CommuneService } from './commune.service';
import { CreateCommuneDto } from './dto/createCommune.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('communes')
@Controller('communes')
export class CommuneController {
    constructor(private readonly communeService: CommuneService) {}

    @Get()
    public async getCountries(@Response() res) {
        const commune = await this.communeService.findAll();
        return res.status(HttpStatus.OK).json(commune);
    }

    @Get('/city/:id')
    public async findCommunesCity(@Response() res, @Param() param) {
        const queryCondition = {'city': param.id};
        const city = await this.communeService.findOptions(queryCondition);
        return res.status(HttpStatus.OK).json(city);
    }

    @Get('/:id')
    public async getCommune(@Response() res, @Param() param){
        const commune = await this.communeService.findById(param.id);
        return res.status(HttpStatus.OK).json(commune);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createCommune(@Response() res, @Body() createCommuneDTO: CreateCommuneDto) {

        const todo = await this.communeService.create(createCommuneDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateCommune(@Param() param, @Response() res, @Body() body) {

        const todo = await this.communeService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteCommune(@Param() param, @Response() res) {

        const todo = await this.communeService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
