import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { StrataService } from './strata.service';
import { CreateStrataDto } from './dto/createStrata.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('stratas')
@Controller('stratas')
export class StrataController {
    constructor(private readonly strataService: StrataService) {}

    @Get()
    public async getCountries(@Response() res) {
        const strata = await this.strataService.findAll();
        return res.status(HttpStatus.OK).json(strata);
    }

    @Get('find')
    public async findStrata(@Response() res, @Body() body) {
        const queryCondition = body;
        const strata = await this.strataService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(strata);
    }

    @Get('/:id')
    public async getStrata(@Response() res, @Param() param){
        const strata = await this.strataService.findById(param.id);
        return res.status(HttpStatus.OK).json(strata);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createStrata(@Response() res, @Body() createStrataDTO: CreateStrataDto) {

        const todo = await this.strataService.create(createStrataDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateStrata(@Param() param, @Response() res, @Body() body) {

        const todo = await this.strataService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteStrata(@Param() param, @Response() res) {

        const todo = await this.strataService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
