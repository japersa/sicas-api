import * as mongoose from 'mongoose';

export const SurveyTypeSchema = new mongoose.Schema({
    description: String
});