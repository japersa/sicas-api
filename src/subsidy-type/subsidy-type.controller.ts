import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { SubsidyTypeService } from './subsidy-type.service';
import { CreateSubsidyTypeDto } from './dto/createSubsidyType.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('subsidy-types')
@Controller('subsidy-types')
export class SubsidyTypeController {
    constructor(private readonly subsidyTypeService: SubsidyTypeService) {}

    @Get()
    public async getCountries(@Response() res) {
        const subsidyType = await this.subsidyTypeService.findAll();
        return res.status(HttpStatus.OK).json(subsidyType);
    }

    @Get('find')
    public async findSubsidyType(@Response() res, @Body() body) {
        const queryCondition = body;
        const subsidyType = await this.subsidyTypeService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(subsidyType);
    }

    @Get('/:id')
    public async getSubsidyType(@Response() res, @Param() param){
        const subsidyType = await this.subsidyTypeService.findById(param.id);
        return res.status(HttpStatus.OK).json(subsidyType);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createSubsidyType(@Response() res, @Body() createSubsidyTypeDTO: CreateSubsidyTypeDto) {

        const todo = await this.subsidyTypeService.create(createSubsidyTypeDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateSubsidyType(@Param() param, @Response() res, @Body() body) {

        const todo = await this.subsidyTypeService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteSubsidyType(@Param() param, @Response() res) {

        const todo = await this.subsidyTypeService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
