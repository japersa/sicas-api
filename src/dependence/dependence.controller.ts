import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { DependenceService } from './dependence.service';
import { CreateDependenceDto } from './dto/createDependence.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('dependences')
@Controller('dependences')
export class DependenceController {
    constructor(private readonly dependenceService: DependenceService) {}

    @Get()
    public async getCountries(@Response() res) {
        const dependence = await this.dependenceService.findAll();
        return res.status(HttpStatus.OK).json(dependence);
    }

    @Get('find')
    public async findDependence(@Response() res, @Body() body) {
        const queryCondition = body;
        const dependence = await this.dependenceService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(dependence);
    }

    @Get('/:id')
    public async getDependence(@Response() res, @Param() param){
        const dependence = await this.dependenceService.findById(param.id);
        return res.status(HttpStatus.OK).json(dependence);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createDependence(@Response() res, @Body() createDependenceDTO: CreateDependenceDto) {

        const todo = await this.dependenceService.create(createDependenceDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateDependence(@Param() param, @Response() res, @Body() body) {

        const todo = await this.dependenceService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteDependence(@Param() param, @Response() res) {

        const todo = await this.dependenceService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
