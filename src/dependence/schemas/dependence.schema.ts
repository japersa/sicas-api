import * as mongoose from 'mongoose';

export const DependenceSchema = new mongoose.Schema({
    description: String
});