import { ApiModelProperty } from '@nestjs/swagger';

export class CreateChargeDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

}