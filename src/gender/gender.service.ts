import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateGenderDto } from './dto/createGender.dto';
import { debug } from 'console';
import { IGenderService, IGender } from './interfaces';

@Injectable()
export class GenderService implements IGenderService {
    constructor(@InjectModel('Gender') private readonly genderModel: Model<IGender>) { }

    async findAll(): Promise<IGender[]> {
        return await this.genderModel.find().exec();
    }

    async findOne(options: object): Promise<IGender> {
        return await this.genderModel.findOne(options).exec();
    }

    async findById(ID: number): Promise<IGender> {
        return await this.genderModel.findById(ID).exec();
    }
    async create(createGenderDto: CreateGenderDto): Promise<IGender> {
        const createdGender = new this.genderModel(createGenderDto);
        return await createdGender.save();
    }

    async update(ID: number, newValue: IGender): Promise<IGender> {
        const gender = await this.genderModel.findById(ID).exec();

        if (!gender._id) {
            debug('gender not found');
        }

        await this.genderModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.genderModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.genderModel.findByIdAndRemove(ID).exec();
            return 'The gender has been deleted';
        }
        catch (err){
            debug(err);
            return 'The gender could not be deleted';
        }
    }
}