import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateCityDto } from './dto/createCity.dto';
import { debug } from 'console';
import { ICityService, ICity } from './interfaces';

@Injectable()
export class CityService implements ICityService {
    constructor(@InjectModel('City') private readonly cityModel: Model<ICity>) { }

    async findAll(): Promise<ICity[]> {
        return await this.cityModel.find().populate('department').exec();
    }

    async findOptions(options: object): Promise<ICity[]> {
        return await this.cityModel.find(options).populate('department').exec();
    }

    async findById(ID: number): Promise<ICity> {
        return await this.cityModel.findById(ID).populate('department').exec();
    }
    async create(createCityDto: CreateCityDto): Promise<ICity> {
        const createdCity = new this.cityModel(createCityDto);
        return await createdCity.save();
    }

    async update(ID: number, newValue: ICity): Promise<ICity> {
        const city = await this.cityModel.findById(ID).exec();

        if (!city._id) {
            debug('city not found');
        }

        await this.cityModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.cityModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.cityModel.findByIdAndRemove(ID).exec();
            return 'The city has been deleted';
        }
        catch (err){
            debug(err);
            return 'The city could not be deleted';
        }
    }
}