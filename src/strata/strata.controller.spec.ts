import { Test, TestingModule } from '@nestjs/testing';
import { StrataController } from './strata.controller';

describe('Strata Controller', () => {
  let controller: StrataController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StrataController],
    }).compile();

    controller = module.get<StrataController>(StrataController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
