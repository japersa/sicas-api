import { ISurveyType } from './survey-type.interface';

export interface ISurveyTypeService {
    findAll(): Promise<ISurveyType[]>;
    findById(ID: number): Promise<ISurveyType | null>;
    findOne(options: object): Promise<ISurveyType | null>;
    create(todos: ISurveyType): Promise<ISurveyType>;
    update(ID: number, newValue: ISurveyType): Promise<ISurveyType | null>;
    delete(ID: number): Promise<string>;
}