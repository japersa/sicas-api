import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { NeighborhoodService } from './neighborhood.service';
import { CreateNeighborhoodDto } from './dto/createNeighborhood.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('neighborhoods')
@Controller('neighborhoods')
export class NeighborhoodController {
    constructor(private readonly neighborhoodService: NeighborhoodService) {}

    @Get()
    public async getNeighborhoods(@Response() res) {
        const neighborhood = await this.neighborhoodService.findAll();
        return res.status(HttpStatus.OK).json(neighborhood);
    }

    @Get('/commune/:id')
    public async findNeighborhoodsCity(@Response() res, @Param() param) {
        const queryCondition = {'commune': param.id};
        const city = await this.neighborhoodService.findOptions(queryCondition);
        return res.status(HttpStatus.OK).json(city);
    }

    @Get('/:id')
    public async getNeighborhood(@Response() res, @Param() param){
        const neighborhood = await this.neighborhoodService.findById(param.id);
        return res.status(HttpStatus.OK).json(neighborhood);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createNeighborhood(@Response() res, @Body() createNeighborhoodDTO: CreateNeighborhoodDto) {

        const todo = await this.neighborhoodService.create(createNeighborhoodDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateNeighborhood(@Param() param, @Response() res, @Body() body) {

        const todo = await this.neighborhoodService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteNeighborhood(@Param() param, @Response() res) {

        const todo = await this.neighborhoodService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
