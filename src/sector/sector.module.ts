import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SectorController } from './sector.controller';
import { SectorService } from './sector.service';
import { SectorSchema } from './schemas/sector.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'Sector', schema: SectorSchema }])],
    controllers: [SectorController],
    providers: [SectorService],
})
export class SectorModule {}