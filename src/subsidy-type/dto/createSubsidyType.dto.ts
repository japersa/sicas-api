import { ApiModelProperty } from '@nestjs/swagger';

export class CreateSubsidyTypeDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

}