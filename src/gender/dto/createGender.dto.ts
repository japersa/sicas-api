import { ApiModelProperty } from '@nestjs/swagger';

export class CreateGenderDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

}