import { IFilter } from './filter.interface';

export interface IFilterService {
    findAll(): Promise<IFilter[]>;
    findById(ID: number): Promise<IFilter | null>;
    findOne(options: object): Promise<IFilter | null>;
    create(todos: IFilter): Promise<IFilter>;
    update(ID: number, newValue: IFilter): Promise<IFilter | null>;
    delete(ID: number): Promise<string>;
}