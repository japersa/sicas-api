import { Document } from 'mongoose';

export interface IStrata extends Document {
    readonly description: string;
}