import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { CityService } from './city.service';
import { CreateCityDto } from './dto/createCity.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('cities')
@Controller('cities')
export class CityController {
    constructor(private readonly cityService: CityService) {}

    @Get()
    public async getCities(@Response() res) {
        const city = await this.cityService.findAll();
        return res.status(HttpStatus.OK).json(city);
    }

    @Get('/department/:id')
    public async findCityDepatment(@Response() res, @Param() param) {
        const queryCondition = {'department': param.id};
        const city = await this.cityService.findOptions(queryCondition);
        return res.status(HttpStatus.OK).json(city);
    }

    @Get('/:id')
    public async getCity(@Response() res, @Param() param){
        const city = await this.cityService.findById(param.id);
        return res.status(HttpStatus.OK).json(city);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createCity(@Response() res, @Body() createCityDTO: CreateCityDto) {

        const todo = await this.cityService.create(createCityDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateCity(@Param() param, @Response() res, @Body() body) {

        const todo = await this.cityService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteCity(@Param() param, @Response() res) {

        const todo = await this.cityService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
