import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { DepartmentService } from './department.service';
import { CreateDepartmentDto } from './dto/createDepartment.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('departments')
@Controller('departments')
export class DepartmentController {
    constructor(private readonly departmentService: DepartmentService) {}

    @Get()
    public async getCountries(@Response() res) {
        const department = await this.departmentService.findAll();
        return res.status(HttpStatus.OK).json(department);
    }

    @Get('/country/:id')
    public async findCityDepatment(@Response() res, @Param() param) {
        const queryCondition = {'country': param.id};
        const city = await this.departmentService.findOptions(queryCondition);
        return res.status(HttpStatus.OK).json(city);
    }

    @Get('/:id')
    public async getDepartment(@Response() res, @Param() param){
        const department = await this.departmentService.findById(param.id);
        return res.status(HttpStatus.OK).json(department);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createDepartment(@Response() res, @Body() createDepartmentDTO: CreateDepartmentDto) {

        const todo = await this.departmentService.create(createDepartmentDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateDepartment(@Param() param, @Response() res, @Body() body) {

        const todo = await this.departmentService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteDepartment(@Param() param, @Response() res) {

        const todo = await this.departmentService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
