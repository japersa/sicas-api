import { ICharge } from './charge.interface';

export interface IChargeService {
    findAll(): Promise<ICharge[]>;
    findById(ID: number): Promise<ICharge | null>;
    findOne(options: object): Promise<ICharge | null>;
    create(todos: ICharge): Promise<ICharge>;
    update(ID: number, newValue: ICharge): Promise<ICharge | null>;
    delete(ID: number): Promise<string>;
}