import { ApiModelProperty } from '@nestjs/swagger';

export class CreatePermissionDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly name: string;

    @ApiModelProperty()
    readonly description: string;
}