import { Document } from 'mongoose';

export interface ISurveyType extends Document {
    readonly description: string;
}