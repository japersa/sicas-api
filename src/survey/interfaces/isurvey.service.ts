import { ISurvey } from './survey.interface';

export interface ISurveyService {
    findAll(): Promise<ISurvey[]>;
    findById(ID: number): Promise<ISurvey | null>;
    findOne(options: object): Promise<ISurvey | null>;
    create(todos: ISurvey): Promise<ISurvey>;
    update(ID: number, newValue: ISurvey): Promise<ISurvey | null>;
    delete(ID: number): Promise<string>;
}