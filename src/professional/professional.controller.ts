import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { ProfessionalService } from './professional.service';
import { CreateProfessionalDto } from './dto/createProfessional.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('professionals')
@Controller('professionals')
export class ProfessionalController {
    constructor(private readonly professionalService: ProfessionalService) {}

    @Get()
    public async getProfessionals(@Response() res) {
        const professional = await this.professionalService.findAll();
        return res.status(HttpStatus.OK).json(professional);
    }

    @Get('find')
    public async findProfessional(@Response() res, @Body() body) {
        const queryCondition = body;
        const professional = await this.professionalService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(professional);
    }

    @Get('/:id')
    public async getProfessional(@Response() res, @Param() param){
        const professional = await this.professionalService.findById(param.id);
        return res.status(HttpStatus.OK).json(professional);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createProfessional(@Response() res, @Body() createProfessionalDTO: CreateProfessionalDto) {

        const todo = await this.professionalService.create(createProfessionalDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateProfessional(@Param() param, @Response() res, @Body() body) {

        const todo = await this.professionalService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteProfessional(@Param() param, @Response() res) {

        const todo = await this.professionalService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
