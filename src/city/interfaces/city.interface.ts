import { Document } from 'mongoose';
import { IDepartment } from 'src/department/interfaces';

export interface ICity extends Document {
    readonly description: string;
    readonly department: IDepartment;
}