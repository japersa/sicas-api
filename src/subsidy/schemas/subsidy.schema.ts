import * as mongoose from 'mongoose';

export const SubsidySchema = new mongoose.Schema({
    description: String,
    subsidy_type: { type: mongoose.Schema.Types.ObjectId, ref: 'SubsidyType' },
});