import * as jwt from 'jsonwebtoken';
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Credentials } from './dto/credentials.dto';
import { IUser } from 'src/user/interfaces';
import { JwtPayload } from './interfaces/jwt-payload.inteface';

@Injectable()
export class AuthService {
    constructor(@InjectModel('User') private readonly userModel: Model<IUser>) { }

    async createToken(credentials: Credentials) {
        const user = await this.userModel.findOne({
			email: credentials.email,
			password: credentials.password
		}).populate('roles');

        if(user){
            const expiresIn = 60 * 60;
            const accessToken = jwt.sign({id: user.id}, "token", {
                expiresIn
            });

            const roles = user.roles;

            return {
                expiresIn,
                accessToken,
                roles
            };
        }
        
        return null;
	}

	async verifyToken(token: string) {

		return new Promise(resolve => {
			jwt.verify(token, "token", decoded => resolve(decoded));
		});
	}

	async validateUser(payload: JwtPayload): Promise<any> {
        return await this.userModel.findById(payload.id).populate('roles').exec();
	}

}