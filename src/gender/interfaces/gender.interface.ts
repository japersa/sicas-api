import { Document } from 'mongoose';

export interface IGender extends Document {
    readonly description: string;
}