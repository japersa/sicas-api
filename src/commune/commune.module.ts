import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CommuneController } from './commune.controller';
import { CommuneService } from './commune.service';
import { CommuneSchema } from './schemas/commune.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'Commune', schema: CommuneSchema }])],
    controllers: [CommuneController],
    providers: [CommuneService],
})
export class CommuneModule {}