import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateDepartmentDto } from './dto/createDepartment.dto';
import { debug } from 'console';
import { IDepartmentService, IDepartment } from './interfaces';

@Injectable()
export class DepartmentService implements IDepartmentService {
    constructor(@InjectModel('Department') private readonly departmentModel: Model<IDepartment>) { }

    async findAll(): Promise<IDepartment[]> {
        return await this.departmentModel.find().populate('country').exec();
    }

    async findOptions(options: object): Promise<IDepartment[]> {
        return await this.departmentModel.find(options).populate('country').exec();
    }

    async findById(ID: number): Promise<IDepartment> {
        return await this.departmentModel.findById(ID).populate('country').exec();
    }
    async create(createDepartmentDto: CreateDepartmentDto): Promise<IDepartment> {
        const createdDepartment = new this.departmentModel(createDepartmentDto);
        return await createdDepartment.save();
    }

    async update(ID: number, newValue: IDepartment): Promise<IDepartment> {
        const department = await this.departmentModel.findById(ID).exec();

        if (!department._id) {
            debug('department not found');
        }

        await this.departmentModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.departmentModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.departmentModel.findByIdAndRemove(ID).exec();
            return 'The department has been deleted';
        }
        catch (err){
            debug(err);
            return 'The department could not be deleted';
        }
    }
}