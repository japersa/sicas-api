import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateCommuneDto } from './dto/createCommune.dto';
import { debug } from 'console';
import { ICommuneService, ICommune } from './interfaces';

@Injectable()
export class CommuneService implements ICommuneService {
    constructor(@InjectModel('Commune') private readonly communeModel: Model<ICommune>) { }

    async findAll(): Promise<ICommune[]> {
        return await this.communeModel.find().populate('city').exec();
    }

    async findOptions(options: object): Promise<ICommune[]> {
        return await this.communeModel.find(options).populate('city').exec();
    }

    async findById(ID: number): Promise<ICommune> {
        return await this.communeModel.findById(ID).populate('city').exec();
    }
    async create(createCommuneDto: CreateCommuneDto): Promise<ICommune> {
        const createdCommune = new this.communeModel(createCommuneDto);
        return await createdCommune.save();
    }

    async update(ID: number, newValue: ICommune): Promise<ICommune> {
        const commune = await this.communeModel.findById(ID).exec();

        if (!commune._id) {
            debug('commune not found');
        }

        await this.communeModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.communeModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.communeModel.findByIdAndRemove(ID).exec();
            return 'The commune has been deleted';
        }
        catch (err){
            debug(err);
            return 'The commune could not be deleted';
        }
    }
}