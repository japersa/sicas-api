import { Test, TestingModule } from '@nestjs/testing';
import { DependenceService } from './dependence.service';

describe('DependenceService', () => {
  let service: DependenceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DependenceService],
    }).compile();

    service = module.get<DependenceService>(DependenceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
