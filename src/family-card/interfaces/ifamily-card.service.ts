import { IFamilyCard } from './family-card.interface';

export interface IFamilyCardService {
    findAll(): Promise<IFamilyCard[]>;
    findById(ID: number): Promise<IFamilyCard | null>;
    findOne(options: object): Promise<IFamilyCard | null>;
    create(todos: IFamilyCard): Promise<IFamilyCard>;
    update(ID: number, newValue: IFamilyCard): Promise<IFamilyCard | null>;
    delete(ID: number): Promise<string>;
}