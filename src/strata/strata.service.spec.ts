import { Test, TestingModule } from '@nestjs/testing';
import { StrataService } from './strata.service';

describe('StrataService', () => {
  let service: StrataService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StrataService],
    }).compile();

    service = module.get<StrataService>(StrataService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
