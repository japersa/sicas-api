import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateDependenceDto } from './dto/createDependence.dto';
import { debug } from 'console';
import { IDependenceService, IDependence } from './interfaces';

@Injectable()
export class DependenceService implements IDependenceService {
    constructor(@InjectModel('Dependence') private readonly dependenceModel: Model<IDependence>) { }

    async findAll(): Promise<IDependence[]> {
        return await this.dependenceModel.find().exec();
    }

    async findOne(options: object): Promise<IDependence> {
        return await this.dependenceModel.findOne(options).exec();
    }

    async findById(ID: number): Promise<IDependence> {
        return await this.dependenceModel.findById(ID).exec();
    }
    async create(createDependenceDto: CreateDependenceDto): Promise<IDependence> {
        const createdDependence = new this.dependenceModel(createDependenceDto);
        return await createdDependence.save();
    }

    async update(ID: number, newValue: IDependence): Promise<IDependence> {
        const dependence = await this.dependenceModel.findById(ID).exec();

        if (!dependence._id) {
            debug('dependence not found');
        }

        await this.dependenceModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.dependenceModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.dependenceModel.findByIdAndRemove(ID).exec();
            return 'The dependence has been deleted';
        }
        catch (err){
            debug(err);
            return 'The dependence could not be deleted';
        }
    }
}