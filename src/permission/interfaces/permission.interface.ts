import { Document } from 'mongoose';

export interface IPermission extends Document {
    readonly name: string;
    readonly description: string;
}