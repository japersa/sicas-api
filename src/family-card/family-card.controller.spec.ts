import { Test, TestingModule } from '@nestjs/testing';
import { FamilyCardController } from './family-card.controller';

describe('FamilyCard Controller', () => {
  let controller: FamilyCardController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FamilyCardController],
    }).compile();

    controller = module.get<FamilyCardController>(FamilyCardController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
