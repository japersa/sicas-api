import { Test, TestingModule } from '@nestjs/testing';
import { SubsidyTypeService } from './subsidy-type.service';

describe('SubsidyTypeService', () => {
  let service: SubsidyTypeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SubsidyTypeService],
    }).compile();

    service = module.get<SubsidyTypeService>(SubsidyTypeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
