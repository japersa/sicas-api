import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { SidewalkService } from './sidewalk.service';
import { CreateSidewalkDto } from './dto/createSidewalk.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('sidewalks')
@Controller('sidewalks')
export class SidewalkController {
    constructor(private readonly sidewalkService: SidewalkService) {}

    @Get()
    public async getSidewalks(@Response() res) {
        const sidewalk = await this.sidewalkService.findAll();
        return res.status(HttpStatus.OK).json(sidewalk);
    }

    @Get('/city/:id')
    public async findSidewalksCity(@Response() res, @Param() param) {
        const queryCondition = {'city': param.id};
        const city = await this.sidewalkService.findOptions(queryCondition);
        return res.status(HttpStatus.OK).json(city);
    }

    @Get('/:id')
    public async getSidewalk(@Response() res, @Param() param){
        const sidewalk = await this.sidewalkService.findById(param.id);
        return res.status(HttpStatus.OK).json(sidewalk);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createSidewalk(@Response() res, @Body() createSidewalkDTO: CreateSidewalkDto) {

        const todo = await this.sidewalkService.create(createSidewalkDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateSidewalk(@Param() param, @Response() res, @Body() body) {

        const todo = await this.sidewalkService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteSidewalk(@Param() param, @Response() res) {

        const todo = await this.sidewalkService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
