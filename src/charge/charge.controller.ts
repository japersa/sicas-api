import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { ChargeService } from './charge.service';
import { CreateChargeDto } from './dto/createCharge.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('charges')
@Controller('charges')
export class ChargeController {
    constructor(private readonly chargeService: ChargeService) {}

    @Get()
    public async getCountries(@Response() res) {
        const charge = await this.chargeService.findAll();
        return res.status(HttpStatus.OK).json(charge);
    }

    @Get('find')
    public async findCharge(@Response() res, @Body() body) {
        const queryCondition = body;
        const charge = await this.chargeService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(charge);
    }

    @Get('/:id')
    public async getCharge(@Response() res, @Param() param){
        const charge = await this.chargeService.findById(param.id);
        return res.status(HttpStatus.OK).json(charge);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createCharge(@Response() res, @Body() createChargeDTO: CreateChargeDto) {

        const todo = await this.chargeService.create(createChargeDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch('/:id')
    public async updateCharge(@Param() param, @Response() res, @Body() body) {

        const todo = await this.chargeService.update(param.id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete('/:id')
    public async deleteCharge(@Param() param, @Response() res) {

        const todo = await this.chargeService.delete(param.id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
