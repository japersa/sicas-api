import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateStrataDto } from './dto/createStrata.dto';
import { debug } from 'console';
import { IStrataService, IStrata } from './interfaces';

@Injectable()
export class StrataService implements IStrataService {
    constructor(@InjectModel('Strata') private readonly strataModel: Model<IStrata>) { }

    async findAll(): Promise<IStrata[]> {
        return await this.strataModel.find().exec();
    }

    async findOne(options: object): Promise<IStrata> {
        return await this.strataModel.findOne(options).exec();
    }

    async findById(ID: number): Promise<IStrata> {
        return await this.strataModel.findById(ID).exec();
    }
    async create(createStrataDto: CreateStrataDto): Promise<IStrata> {
        const createdStrata = new this.strataModel(createStrataDto);
        return await createdStrata.save();
    }

    async update(ID: number, newValue: IStrata): Promise<IStrata> {
        const strata = await this.strataModel.findById(ID).exec();

        if (!strata._id) {
            debug('strata not found');
        }

        await this.strataModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.strataModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.strataModel.findByIdAndRemove(ID).exec();
            return 'The strata has been deleted';
        }
        catch (err){
            debug(err);
            return 'The strata could not be deleted';
        }
    }
}