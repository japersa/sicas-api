import { Test, TestingModule } from '@nestjs/testing';
import { DependenceController } from './dependence.controller';

describe('Dependence Controller', () => {
  let controller: DependenceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DependenceController],
    }).compile();

    controller = module.get<DependenceController>(DependenceController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
