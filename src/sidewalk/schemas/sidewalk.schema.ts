import * as mongoose from 'mongoose';

export const SidewalkSchema = new mongoose.Schema({
    description: String,
    city: { type: mongoose.Schema.Types.ObjectId, ref: 'City' },
});