import { IUser } from './user.interface';

export interface IUserService {
    findAll(): Promise<IUser[]>;
    findById(ID: number): Promise<IUser | null>;
    findOne(options: object): Promise<IUser | null>;
    create(todos: IUser): Promise<IUser>;
    update(ID: number, newValue: IUser): Promise<IUser | null>;
    delete(ID: number): Promise<string>;
}