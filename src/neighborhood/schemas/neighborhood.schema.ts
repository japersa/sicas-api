import * as mongoose from 'mongoose';

export const NeighborhoodSchema = new mongoose.Schema({
    description: String,
    commune: { type: mongoose.Schema.Types.ObjectId, ref: 'Commune' },
});