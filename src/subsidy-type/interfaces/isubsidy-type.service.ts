import { ISubsidyType } from './subsidy-type.interface';

export interface ISubsidyTypeService {
    findAll(): Promise<ISubsidyType[]>;
    findById(ID: number): Promise<ISubsidyType | null>;
    findOne(options: object): Promise<ISubsidyType | null>;
    create(todos: ISubsidyType): Promise<ISubsidyType>;
    update(ID: number, newValue: ISubsidyType): Promise<ISubsidyType | null>;
    delete(ID: number): Promise<string>;
}