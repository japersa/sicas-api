import { ApiModelProperty } from '@nestjs/swagger';
import { IRole } from 'src/role/interfaces';

export class Credentials {
    @ApiModelProperty()
    readonly email: string;

    @ApiModelProperty()
    readonly password: string;
}