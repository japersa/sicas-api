import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateNeighborhoodDto } from './dto/createNeighborhood.dto';
import { debug } from 'console';
import { INeighborhoodService, INeighborhood } from './interfaces';

@Injectable()
export class NeighborhoodService implements INeighborhoodService {
    constructor(@InjectModel('Neighborhood') private readonly neighborhoodModel: Model<INeighborhood>) { }

    async findAll(): Promise<INeighborhood[]> {
        return await this.neighborhoodModel.find().populate('commune').exec();
    }

    async findOptions(options: object): Promise<INeighborhood[]> {
        return await this.neighborhoodModel.find(options).populate('commune').exec();
    }

    async findById(ID: number): Promise<INeighborhood> {
        return await this.neighborhoodModel.findById(ID).populate('commune').exec();
    }
    async create(createNeighborhoodDto: CreateNeighborhoodDto): Promise<INeighborhood> {
        const createdNeighborhood = new this.neighborhoodModel(createNeighborhoodDto);
        return await createdNeighborhood.save();
    }

    async update(ID: number, newValue: INeighborhood): Promise<INeighborhood> {
        const neighborhood = await this.neighborhoodModel.findById(ID).exec();

        if (!neighborhood._id) {
            debug('neighborhood not found');
        }

        await this.neighborhoodModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.neighborhoodModel.findById(ID).exec();
    }
    async delete(ID: number): Promise<string> {
        try {
            await this.neighborhoodModel.findByIdAndRemove(ID).exec();
            return 'The neighborhood has been deleted';
        }
        catch (err){
            debug(err);
            return 'The neighborhood could not be deleted';
        }
    }
}