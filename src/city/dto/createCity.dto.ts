import { ApiModelProperty } from '@nestjs/swagger';
import { IDepartment } from 'src/department/interfaces';

export class CreateCityDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly description: string;

    @ApiModelProperty()
    readonly department: IDepartment;
}