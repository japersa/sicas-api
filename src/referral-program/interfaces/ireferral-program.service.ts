import { IReferralProgram } from './referral-program.interface';

export interface IReferralProgramService {
    findAll(): Promise<IReferralProgram[]>;
    findById(ID: number): Promise<IReferralProgram | null>;
    findOne(options: object): Promise<IReferralProgram | null>;
    create(todos: IReferralProgram): Promise<IReferralProgram>;
    update(ID: number, newValue: IReferralProgram): Promise<IReferralProgram | null>;
    delete(ID: number): Promise<string>;
}