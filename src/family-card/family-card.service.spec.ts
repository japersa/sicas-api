import { Test, TestingModule } from '@nestjs/testing';
import { FamilyCardService } from './family-card.service';

describe('FamilyCardService', () => {
  let service: FamilyCardService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FamilyCardService],
    }).compile();

    service = module.get<FamilyCardService>(FamilyCardService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
