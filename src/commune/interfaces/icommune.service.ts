import { ICommune } from './commune.interface';

export interface ICommuneService {
    findAll(): Promise<ICommune[]>;
    findById(ID: number): Promise<ICommune | null>;
    findOptions(options: object): Promise<ICommune[]>;
    create(todos: ICommune): Promise<ICommune>;
    update(ID: number, newValue: ICommune): Promise<ICommune | null>;
    delete(ID: number): Promise<string>;
}