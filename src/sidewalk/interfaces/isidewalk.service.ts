import { ISidewalk } from './sidewalk.interface';

export interface ISidewalkService {
    findAll(): Promise<ISidewalk[]>;
    findById(ID: number): Promise<ISidewalk | null>;
    findOptions(options: object): Promise<ISidewalk[]>;
    create(todos: ISidewalk): Promise<ISidewalk>;
    update(ID: number, newValue: ISidewalk): Promise<ISidewalk | null>;
    delete(ID: number): Promise<string>;
}