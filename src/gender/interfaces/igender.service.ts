import { IGender } from './gender.interface';

export interface IGenderService {
    findAll(): Promise<IGender[]>;
    findById(ID: number): Promise<IGender | null>;
    findOne(options: object): Promise<IGender | null>;
    create(todos: IGender): Promise<IGender>;
    update(ID: number, newValue: IGender): Promise<IGender | null>;
    delete(ID: number): Promise<string>;
}